<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\appModels\BreakingNewsSearch;
use app\controllers\ActionDataProvider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->distinct(),
        ]);
        
        return  $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"Select count(dorsal) from ciclista",
        ]);
    }
    
    public function actionConsulta1b(){
        $dataProvider = new SqlDataProvider([
        'sql'=>'Select count(dorsal) from ciclista',
        ]);
        return  $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"Select count(dorsal) from ciclista",
        ]);
    }
    public function actionConsulta2(){
        $dataProvider= new ActiveDataProvider([
            'query'=> Ciclista::find()->select("dorsal")->distinct()->where("nomequipo='Banesto'"),
        ]);
        
         return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"Consulta 2 con Active record",
        "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
        "sql"=>"Select count(dorsal) from ciclista where nomequipo='Banesto'",
    ]); 
    }
    
    public function actionConsulta2b(){
        
        $dataProvider =new SqlDataProvider([
        'sql'=>'Select count(dorsal) from ciclista where nomequipo="Banesto"',
        ]);
    return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"Consulta 2 con DAO",
        "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
        "sql"=>"Select count(dorsal) from ciclista where nomequipo='Banesto'",
    ]);
    }
    
    public function actionConsulta3(){
        $dataProvider= new ActiveDataProvider([
           'query' => Ciclista::find()->select("avg(edad)")->distinct(),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>["avg(edad)"],
            "titulo"=>"Consulta 3 con Active record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"Select distinct avg(edad) from ciclista",
       ]); 
    }
    
    public function actionConsulta3b(){
        $dataProvider= new SqldataProvider([
        'sql'=>'Select distinct avg(edad) from ciclista',
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>["avg(edad)"],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"Select distinct avg(edad) from ciclista",  
        ]);
    }
    
    public function actionConsulta4(){
        $dataProvider= new ActiveDataProvider([
           'query' => Ciclista::find()->select("avg(edad)")->distinct()->where("nomequipo=Banesto"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['avg(edad)'],
            "titulo"=>"Consulta 4 con Active record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"Select avg(edad) from ciclista where nomequipo='Banesto'",  
        ]);
    }
    public function actionConsulta4b(){
        $dataProvider= new SqldataProvider([
        'sql'=>'Select avg(edad) from ciclista where equipo="Banesto"',
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['avg(edad)'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"Select avg(edad) from ciclista where equipo='Banesto'",  
        ]);
    }
    
    public function actionConsulta5(){
        $dataProvider=new ActiveDataProvider([
         'query'=> Ciclista::find()->select("avg(edad)")->distinct()->groupBy("nomequipo"),
        ]);
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['avg(edad)'],
            "titulo"=>"Consulta 5 con Active record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"Select avg(edad) from ciclista group by nomequipo",
        ]);       
    }
    
    public function actionConsulta5b(){
        $dataProvider= new SqldataProvider([
         'query'=> 'Select avg(edad) from ciclista group by nomequipo',
        ]);
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['avg(edad)'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"Select avg(edad) from ciclista group by nomequipo",
        ]);       
    }
    
    public function actionConsulta6(){
        $dataProvider=new ActiveDataProvider([
         'query'=> Ciclista::find()->select('count(dorsal),nomequipo')->distinct()->groupBy('nomequipo'),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['count(dorsal)'],
            "titulo"=>'Consulta 6 con DAO',
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"Select count(dorsal),nomequipo form ciclista group by nomequipo",
        ]);      
    }
    
    
    public function actionConsulta6b(){
        $dataProvider= new SqldataProvider([
        'sql'=>'Select count(dorsal),nomequipo from ciclista group by nomequipo',
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['count(dorsal)'],
            "titulo"=>'Consulta 6 con Active record',
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"Select count(dorsal),nomequipo form ciclista group by nomequipo",
        ]);
    }
    
    public function actionConsulta7(){
        $dataProvider=new ActiveDataProvider([
         'query'=>Puerto::find()->select("count(dorsal)")->distinct(),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['count(dorsal)'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"select count(nompuerto) from puerto",
        ]);
    }
    
    public function actionConsulta7b(){
        $dataProvider= new SqldataProvider([
        'sql'=>'select count(nompuerto) from puerto',
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['count(dorsal)'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"select count(nompuerto) from puerto",
        ]);
    }
    
    
    public function actionConsulta8(){
        $dataProvider= new ActiveDataProvider([
          'query'=>Puerto::find()->select("count(nompuerto)")->distinct()->where("altura>1500"), 
        ]);
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['count(nompuerto)'],
            "titulo"=>"Consulta 8 con Active record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"select count(nompuerto) from puerto where  altura>1500",
        ]);
    }
    
    public function actionConsulta8b(){
        $dataProvider= new SqldataProvider([
        'sql'=>'select count(nompuerto) from puerto where  altura>1500',
        ]);
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['count(nompuertol)'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"select count(nompuerto) from puerto where  altura>1500",
        ]);
    }
    
    public function actionConsulta9(){
        $dataProvider= new ActiveDataProvider([
         'query'=>Ciclista::find()->select("nomequipo")->distinct()->groupBy("nomequipo having count(dorsal)>4"),   
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Active record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"select nomequipo from ciclista group by nomequipo having count(dorsal)>4",
        ]);
    }
    
    public function actionConsulta9b(){
        $dataProvider= new SqldataProvider([
        'sql'=>'select nomequipo from ciclista group by nomequipo having count(dorsal)>4',
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"select nomequipo from ciclista group by nomequipo having count(dorsal)>4",
        ]);
    }
    
    public function actionConsulta10(){
        $dataProvider= new ActiveDataProvider([
         'query'=>Ciclista::find()->select("nomequipo")->distinct()->where("edad between 28 and 32")->groupBy("nomequipo having count(dorsal)>4"),   
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con Active record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32",
            "sql"=>"select nomequipo from ciclista where edad between 28 and 32 group by nomequipo having count(dorsal)>4",
        ]);
    }
    public function actionConsulta10b(){
        $dataProvider= new SqldataProvider([
        'sql'=>'select nomequipo from ciclista where edad between 28 and 32 group by nomequipo having count(dorsal)>4',
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32",
            "sql"=>"select nomequipo from ciclista where edad between 28 and 32 group by nomequipo having count(dorsal)>4",
        ]);
    }
    
    public function actionConsulta11(){
        $dataProvider= new ActiveDataProvider([
         'query'=>Lleva::find()->select("count(dorsal),dorsal")->distinct()->where("código='MGE'")->groupBy("dorsal"),   
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['count(dorsal),dorsal'],
            "titulo"=>"Consulta 11 con Active record",
            "enunciado"=>"Indícame el número de estapas que ha ganado cada uno de los ciclistas",
            "sql"=>"select count(dorsal),dorsal from lleva where código='MGE' group by dorsal",
        ]);
    }
    public function actionConsulta11b(){
        $dataProvider= new SqldataProvider([
        'sql'=>'select count(dorsal),dorsal from lleva where código="MGE" group by dorsal',
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['count(dorsal),dorsal'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de estapas que ha ganado cada uno de los ciclistas",
            "sql"=>"select count(dorsal),dorsal from lleva where código='MGE' group by dorsal",
        ]);
    }
    
    public function actionConsulta12(){
        $dataProvider= new ActiveDataProvider([
         'query'=>Lleva::find()->select("dorsal")->distinct()->where("código='MGE'")->groupBy("dorsal having count(*)>1"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con Active record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"select dorsal from lleva where código='MGE' group by dorsal having count(*)>1",
        ]);
    }
    public function actionConsulta12b(){
        $dataProvider= new SqldataProvider([
        'sql'=>'select dorsal from lleva where código="MGE" group by dorsal having count(*)>1',
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"select dorsal from lleva where código='MGE' group by dorsal having count(*)>1",
        ]);
    }
    
    
}

